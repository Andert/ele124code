﻿using System;
using System.Net;

namespace Oving_4_9
{
    public class Computer: INetworkUnit
    {
        public Computer(string name, CustomIPAddress iPAddress)
        {
            this.Name = name;
            this.IPAddress = iPAddress;
        }

        public CustomIPAddress IPAddress {get; set;}
        public string Name { get; set; }


        public override string ToString()
        {
            return $"{this.Name} - {IPAddress}";
        }

    }
}