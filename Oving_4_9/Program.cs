﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Oving_4_9
{
    class Program
    {
        static void Main(string[] args)
        {
            int netMask = 22;
            CustomIPAddress myIP = new CustomIPAddress("192.167.23.14", 22);
            Computer c = new Computer("MyComputer",myIP);
            Console.WriteLine(c.IPAddress.ToString());
        }
    }
}
