﻿using System;
using System.Net;

namespace Oving_4_9
{
    public interface INetworkUnit
    {
        CustomIPAddress IPAddress { get; set; }
    }
}