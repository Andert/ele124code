﻿using System;

namespace Oving_4_9
{
    public class CustomIPAddress
    {
        public CustomIPAddress(UInt32 address, int maskSize)
        {
            this.Address = address;
            this.NetMaskSize = maskSize;
        }

        public CustomIPAddress(string adressString, int maskSize)
        {
            this.NetMaskSize = maskSize;
            string[] groups = adressString.Split('.');
            foreach (var i in groups)
            {
                Address = Address << 8;
                int groupNumber = int.Parse(i);
                if (groupNumber < 0 || groupNumber > 255)
                {
                    throw new ArgumentException("IPaddress: Wrong number in one of the groups.");
                }
                Address += (UInt32)groupNumber;
            }
        }

        private int netMaskSize;
        public int NetMaskSize
        {
            get { return this.netMaskSize; }
            set
            {
                if (value < 0 || value >= 32)
                {
                    throw new ArgumentException("Mask size needs to be between 1-32");
                }
                this.netMaskSize = value;
            }
        }

        public UInt32 NetworkAddress
        {
            get
            {
                UInt32 address = this.Address;
                address >>= this.NetMaskSize;
                address <<= this.NetMaskSize;
                return address;
            }
        }

        public UInt32 Address { get; set; }

        private static string AddressToString(UInt32 address)
        {
            string returnString = "";
            UInt32 tempAddress = address;
            for (int i = 0; i < 4; i++)
            {
                uint groupOf8Bits = tempAddress >> (8 * i);
                groupOf8Bits &= 0xFF;
                returnString = groupOf8Bits.ToString() + '.' + returnString;
            }
            return returnString.TrimEnd('.');
        }

        public override string ToString()
        {
            return $"{AddressToString(NetworkAddress)} / {NetMaskSize} - {AddressToString(Address)}";
        }
    }
}