﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary56;

namespace _2013_12_13_oppg5
{
    class Program
    {
        static void Main(string[] args)
        {
            string regNumber = "SK123456";
            int passID = 4512;
            CarWAutoPass myCar = new CarWAutoPass(regNumber, passID);
            Car shittyCar = new Car("SK700536");
            string location = "Bergen-Damsgård";
            int tollID = 312;
            TollStation bergenBomStasjon = new TollStation(tollID, location);
            Passing myPassing = new PassingWAutoPass(myCar, bergenBomStasjon);
            Console.ReadLine();
            Passing differentPassing = new PassingWOAutoPass(shittyCar, bergenBomStasjon, "photoOfShittyCar");
            Console.WriteLine(myPassing);
            Console.WriteLine(differentPassing);
            Console.ReadLine();
        }
    }
}
