﻿// Filnavn: oppg2a_start.cs

using System;
namespace oppg2a
{
    class Program
    {
        static void Main(string[] args)
        {
            KarakterBok min = new KarakterBok();
            Console.WriteLine("Kursets navn initielt: '{0}'", min.KursNavn);
            Console.Write("Oppgi kursets navn: ");
            min.KursNavn = Console.ReadLine();
            Console.Write("Oppgi karakter: ");
            min.Karakter = char.Parse(Console.ReadLine());
            min.VisMelding();
            Console.WriteLine("Trykk en tast ...");
            Console.ReadKey();
        }
    }
}
