﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oppg3_2016106
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string[] lines = File.ReadAllLines(@"../../oppg3-data.txt");
                List<double[]> allValues = ConvertStringValues(lines);
                List<string> utFilLinjer = GenerateOutFileLines(allValues);
                double totalMean = CalculateSum(allValues)/(allValues.Count*2);
                WriteAlert(allValues, 0.5);

                File.WriteAllLines(@"../../oppg3-utData.txt", utFilLinjer.ToArray());
                Double min = CalculateMin(allValues);
                Double max = CalculateMax(allValues);
                Console.WriteLine($"Skrev ut {utFilLinjer.Count} linjer\n" +
                    $"gjennomsnittet av alle målingene er: {totalMean}\n" +
                    $"min: {min}\tmax: {max}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message+"\n Press a key to exit");
                Console.ReadKey();
            }
        }

        private static void WriteAlert(List<double[]> allValues, double limit)
        {
            foreach (double[] valuePair in allValues)
            {
                if (Math.Abs(valuePair[0] - valuePair[1]) > limit)
                {
                    Console.WriteLine($"ALERT: high diff between {valuePair[0]} and {valuePair[1]}");
                }
            }
        }

        private static double CalculateMax(List<double[]> allValues)
        {
            double max = allValues[0][0];
            foreach (double[] valuePair in allValues)
            {
                max = Math.Max(max,Math.Max(valuePair[0], valuePair[1]));
            }
            return max;
        }

        private static double CalculateMin(List<double[]> allValues)
        {
            double min = allValues[0][0];
            foreach (double[] valuePair in allValues)
            {
                min = Math.Min(min,Math.Min(valuePair[0], valuePair[1]));
            }
            return min;
        }

        private static double CalculateSum(List<double[]> allValues)
        {
            double sum = 0;
            foreach (double [] numberPair in allValues)
            {
                sum += numberPair[0] + numberPair[1];
            }
            return sum;
        }

        private static List<string> GenerateOutFileLines(List<double[]> allValues)
        {
            List<string> returnString = new List<string>();
            foreach (double[] valuePair in allValues)
            {
                double meanTemp = (valuePair[0] + valuePair[1]) / 2.0;
                string utFilLinje = $"({valuePair[0]}+{valuePair[1]})/2 = {meanTemp}.";
                returnString.Add(utFilLinje);
            }
            return returnString;
        }

        private static List<double[]> ConvertStringValues(string[] lines)
        {
            List<double[]> returnList = new List<double[]>();
            for (int i = 2; i < lines.Length; i++)
            {
                string[] values = lines[i].Split(' ');
                double aValue = double.Parse(values[0]);
                double bValue = double.Parse(values[1]);
                returnList.Add(new double[] { aValue, bValue });
            }
            return returnList;
        }
    }
}
