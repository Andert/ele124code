﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFsandbox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Product> myProducts;
        public MainWindow()
        {
            InitializeComponent();

            myProducts = new ObservableCollection<Product>
            {
                new Product("Burger", 24),
                new Product("Banana", 6)
            };

            productList.ItemsSource = myProducts;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double price;
            if (Double.TryParse(priceTextBox.Text, out price))
            {
                myProducts.Add(new Product(nameTextBox.Text, price));
            }
        }
    }
    class Product
    {
        static int sId = 0;
        public Product(string name, double price)
        {
            this.Id += sId++;
            this.Name = name;
            this.Price = price;
        }

        public int Id { get; private set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
