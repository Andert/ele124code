﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> myWords = new Dictionary<int, string>
            {
                { 3, "fizz" },
                { 5, "buzz" },
            };

            for (int i = 1; i <= 100; i++)
            {
                string output = "";
                foreach (var item in myWords)
                {
                    if (i % item.Key == 0)
                    {
                        output += item.Value;
                    }
                }
                if (output == "")
                {
                    output = i.ToString();
                }
                Console.WriteLine(output);
            }
        }
    }
}
