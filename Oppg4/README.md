# Forklaring til løsningen

Mange endringer er gjort fra den orginale oppgaven.

En bussTur aggregerer en Bus og en Linje. For meg er det logisk at bussen velger hvilken linje den skal kjøre på.
Dette gjør det lettere for en buss å bytte linjer.

En linje (Route) har en liste med holdeplasser og ikke motsatt (en holdeplass har en Linje) Hvis Linjen blir slettet, blir ikke stoppene sletten (Aggregation)
* Klassen Linje rommer alle busslinjene:
* ~~En Linje består av mange Turer.~~ En Tur (BusTrip) aggregerer en linje (Line).

      Dette er fordi en Tur som består av en Bus skal kunne bytte linje.

* Klassen Tur rommer alle enkeltturer (én fysisk buss med sjåfør). Eksempel:

      Her har jeg fjernet retning og tenker heller at det trengs en linje (Route) for hver retning.
      I tillegg har jeg lagt til ett felt Linje (Line)
* ~~Dersom en tur har retning ”fram”, betyr det at den går fra terminal A til terminal B.~~
* ”avgangstid” er avgangstiden fra start-terminalen.
* ”forsinkelse” er i utgangspunktet 0 minutter. Du kan forutsette at denne forsinkelsen gjelder for alle holdeplassene på turen.
* Klassen Holdeplass rommer alle stoppene, angitt med et navn.

      Holdeplass (Stop) inneholder kun navn, greit å ha som egen klasse i tilfelle utvidelser (GPS koordinater f.eks)
      RuteStop (RouteStop) Inneholder ett Stop i tillegg til hvor lang tid det tar  kjøre til det stoppet.
* ~~Du kan forutsette at en Holdeplass alltid er knyttet til én Linje og at den alltid finnes på begge sider av veien.~~

      En RouteStop inneholder en oversikt over hvilke busser (BusTrip) som besøker den. Dette er Aggregering
      Litt "Tricky" Siden dette må oppdateres når BustTrip oppdaterer Ruten sin, men dette lar oss finne alle ankomster
      for en gitt holdeplass. (mulig dette burde vært flyttet over på RouteStop)
* ~~En Tur består av mange Avgangstider og en Avgangstid er knyttet til én Holdeplass.~~
      
      Det er ikke Avgangstiden sin oppgave å holde rede på hvilken holdeplass den er knyttet til.
      Dette burde i så fall vært motsatt. Abstraksjonen bør gå nedover.

* Ett RouteStop inneholder hvor langt unna det er.
* BusTrip kan kalkulere alle avgangstider ved hjelp av metoden Times().
* ~~Klassen Avgangstid rommer de enkelte tidspunktene en buss skal ankomme en Holdeplass.~~
      
      Igjen så burde dette ligge i en annen klasse.

