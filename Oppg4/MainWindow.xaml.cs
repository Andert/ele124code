﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oppg4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Route> myRoutes;
        List<BusTrip> myBuses;
        Dictionary<string, Stop> myStops;
        public MainWindow()
        {
            InitializeComponent();

            myStops = ReadAllStops(@"..\..\allStops.csv");
            allStopsListBox.ItemsSource = myStops.Values;
            myRoutes = new List<Route>();
            myRoutes.Add(new Route(3, AddRouteFromFile(0, @"..\..\route3Stops.csv")));
            myRoutes.Add(new Route(4, AddRouteFromFile(0, @"..\..\route4Stops.csv")));

            routesListBox.ItemsSource = myRoutes;

            InitializeBuses();
            busesListBox.ItemsSource = myBuses;
        }

        private Dictionary<string, Stop> ReadAllStops(string fileName)
        {
            Dictionary<string, Stop> allStops = new Dictionary<string, Stop>();
            string[] lines = System.IO.File.ReadAllLines(fileName);
            foreach (var line in lines.Skip(1))
            {
                allStops.Add(line, new Stop(line));
            }
            return allStops;
        }

        private void InitializeBuses()
        {
            myBuses = new List<BusTrip>
            {
                new BusTrip(myRoutes[0]) {Bus = new Bus("Olga"), Departure = new TimeSpan(7, 32, 0)},
                new BusTrip(myRoutes[0]) {Bus = new Bus("Per"), Departure = new TimeSpan(8,42,0)},
                new BusTrip(myRoutes[1]) {Bus = new Bus("Line"), Departure= new TimeSpan(9,30,00)}
            };
        }

        private List<RouteStop> AddRouteFromFile(int myRouteIndex, string fileName)
        {
            List<RouteStop> routeStops = new List<RouteStop>();
            string[] lines = System.IO.File.ReadAllLines(fileName);
            foreach (var line in lines.Skip(1))
            {
                string stopName = line.Split(',')[0];
                string timeString = line.Split(',')[1];
                TimeSpan timeSpanTemp = TimeSpan.Parse(timeString);
                routeStops.Add(new RouteStop(myStops[stopName], timeSpanTemp));
            }
            return routeStops;
        }

        private void routesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            stopsListBox.ItemsSource = ((Route)routesListBox.SelectedItem).Stops;
        }

        private void busesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bussStopsRealTime.ItemsSource = ((BusTrip)busesListBox.SelectedItem).Times();
            busDelaySlider.Value = ((BusTrip)busesListBox.SelectedItem).Delay.Minutes;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int minutes = (int)((Slider)sender).Value;
            myBuses[busesListBox.SelectedIndex].Delay = new TimeSpan(0, minutes, 0);
            busDelay.Text = $"Delay: {minutes}";
            bussStopsRealTime.ItemsSource = ((BusTrip) busesListBox.SelectedItem).Times();
            UpdateVisitingTimes(((Stop)allStopsListBox.SelectedItem));
        }

        private void allStopsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Stop selectedStop = ((Stop)allStopsListBox.SelectedItem);
            visitsBusesListBox.ItemsSource = selectedStop.Visits;
            UpdateVisitingTimes(selectedStop);
        }

        private void UpdateVisitingTimes(Stop selectedStop)
        {
            List<TimeTableEntry> times = new List<TimeTableEntry>();
            try
            {
                foreach (BusTrip busTrip in ((Stop)allStopsListBox.SelectedItem).Visits)
                {
                    times.Add(busTrip.GetTime(selectedStop));
                }

                visitsTimesListBox.ItemsSource = times;
            }
            catch (NullReferenceException ex){} //In case nothing is selected.
        }
    }


}
