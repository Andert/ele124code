﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Oppg4
{
    public class Route
    {
        private List<RouteStop> stops;
        private int routeID;

        public Route(int routeID, List<RouteStop> stops)
        {
            this.RouteID = routeID;
            this.stops = stops;
        }

        public List<RouteStop> Stops { get => stops;}
        public int RouteID { get => routeID; set => routeID = value; }

        public override string ToString()
        {
            return $"{RouteID} :{stops[0]} - {stops[stops.Count-1]}";
        }
    }

    public struct RouteStop
    {
        Stop stop;
        TimeSpan timeFromStart;
        public TimeSpan TimeFromStart { get => timeFromStart; }
        public Stop Stop { get => stop; }

        public RouteStop(Stop stop, TimeSpan timeFromStart)
        {
            this.stop = stop;
            this.timeFromStart = timeFromStart;
        }

        public override string ToString()
        {
            return $"{Stop} - {TimeFromStart}";
        }
    }
    public class Stop
    {
        private string name;
        public Stop(string name)
        {
            this.name = name;
            this.Visits = new List<BusTrip>();
        }

        public string Name { get => name;}
        public List<BusTrip> Visits { get;}

        public override string ToString()
        {
            return $"{Name}";
        }
    }

    public class BusTrip
    {
        public BusTrip(Route route)
        {
            this.route = route;
            foreach (RouteStop routeStop in this.route.Stops)
            {
                routeStop.Stop.Visits.Add(this);
            }
        }

        public Bus Bus
        { get; set; }
        public TimeSpan Departure { get; set; }
        public TimeSpan Delay { get; set; }

        private Route route;
        public Route Route {
            get { return route; }
            set
            {
                foreach (RouteStop routeStop in this.route.Stops)
                {
                    routeStop.Stop.Visits.Remove(this);
                }
                this.route = value;
                foreach (RouteStop routeStop in this.route.Stops)
                {
                    routeStop.Stop.Visits.Add(this);
                }
            }
        }

        public List<TimeTableEntry> Times()
        {
            List<TimeTableEntry> times = new List<TimeTableEntry>();
            foreach (RouteStop routeStop in Route.Stops)
            {
                times.Add(new TimeTableEntry(routeStop.Stop, routeStop.TimeFromStart + this.Departure, routeStop.TimeFromStart + this.Departure + this.Delay));
            }
            return times;
        }

        public TimeTableEntry GetTime(Stop stop)
        {
            RouteStop routeStop = Route.Stops.Find(s => s.Stop == stop);
            return new TimeTableEntry(stop, routeStop.TimeFromStart + this.Departure, routeStop.TimeFromStart + this.Departure + this.Delay);
        }

        public override string ToString()
        {
            return $"Route:{Route.RouteID}, ID:{this.Bus.BusID}, Driver:{this.Bus.Driver}";
        }
    }

    public class Bus
    {
        static int sBusId = 0;
        public Bus(string driver)
        {
            this.BusID = sBusId++;
            this.Driver = driver;
        }

        public string Driver { get; set; }
        public int BusID { get; private set; }

    }


    public struct TimeTableEntry
    {
        private Stop busStop;
        private TimeSpan tableArrival;
        private TimeSpan realTimeArrival;

        public TimeTableEntry(Stop stop, TimeSpan tableArrival, TimeSpan realTimeArrival)
        {

            this.busStop = stop;
            this.tableArrival = tableArrival;
            this.realTimeArrival = realTimeArrival;
        }

        public override string ToString()
        {
            return $"{this.busStop.Name} {this.tableArrival} (RealTime:{this.realTimeArrival}";
        }
    }

    
}