﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2013_12_13_oppg4
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string[] linesA = File.ReadAllLines(@"../../oppg4-start.txt");
                List<DataValue> dataValues = GenerateDatavalues(linesA);
                List<string> toFile = GenerateStrings(dataValues);
                File.WriteAllLines(@"../../oppg4.txt", toFile.ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }

        private static List<string> GenerateStrings(List<DataValue> dataValues)
        {
            List<string> returnStrings = new List<string>();
            DataValue firstValue = dataValues[0];
            double prevTemp = firstValue.Value;
            string prevTime = firstValue.Time;
            foreach (var dataValue in dataValues.Skip(1))
            {
                double diff = prevTemp- dataValue.Value;
                if (Math.Abs(diff) > 2.0)
                {
                    throw new Exception($"Too high difference in temperature{dataValue.Time}");
                }
                prevTemp = dataValue.Value;
                prevTime = dataValue.Time;
                returnStrings.Add($"{prevTime} - {dataValue.Time} thisTemp:{prevTemp}, nextTemp:{dataValue.Value} Diff:{diff}");
            }

            return returnStrings;
        }

        private static List<DataValue> GenerateDatavalues(string[] linesA)
        {
            List<DataValue> dataValues = new List<DataValue>();
            foreach (var line in linesA)
            {
                string[] values = line.Split(' ');
                dataValues.Add(new DataValue(values[0], Double.Parse(values[1])));
            }
            return dataValues;
        }
    }

    struct DataValue
    {
        public string Time { get; private set; }
        public double Value { get; private set; }

        public DataValue(string time, double value) : this()
        {
            this.Time = time;
            this.Value = value;
        }
    }
}
