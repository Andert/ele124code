﻿using System;

namespace Tid
{
    public class Tid
    {
        private TimeSpan time;
        private TimeSpan Time
        {
            get { return time; }
            set
            {
                time = value;// - TimeSpan.FromDays(value.Days);
            }
        }

        public Tid() :this(0,0,0) { }
        public Tid(int hour) : this(hour, 0, 0){}
        public Tid(int hour, int minute) : this(hour, minute, 0){}
        public Tid(int hour, int minute, int second)
        {
            this.Time = new TimeSpan(hour, minute, second);
        }
        public Tid(Tid otherTid)
        {
            this.Time = otherTid.Time;
        }
        public Tid(TimeSpan timeSpan)
        {
            this.Time = timeSpan;
        }

        public override string ToString()
        {
            return String.Format("{ 0}h: { 1:D2}m: { 2:D2}s", (int)Time.TotalHours, Time.Minutes, Time.Seconds);
        }

        public static Tid Now()
        {
            return new Tid(DateTime.Now.TimeOfDay);
        }
    } 
}
