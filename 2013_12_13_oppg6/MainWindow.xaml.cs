﻿using ClassLibrary56;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _2013_12_13_oppg6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static List<Passing> myPassings = new List<Passing>();
        public MainWindow()
        {
            List<Car> myCars = new List<Car>
            {
                new Car("SK1234"),
                new CarWAutoPass("SV12345", 55555),
                new Car("RK19239"),
                new CarWAutoPass("TACO",9999)
            };

            List<TollStation> myTolls = new List<TollStation>
            {
                new TollStation(1,"Bergen"),
                new TollStation(313,"Stavanger"),
                new TollStation(91,"Oslo"),
                new TollStation(11,"Tromsø")
            };

            InitializeComponent();
            this.AddButton.Click += AddButton_Click;
            CarsListBoX.ItemsSource = myCars;
            TollListBoX.ItemsSource = myTolls;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Car selectedCar = (Car)CarsListBoX.SelectedItem;
            TollStation selectedToll = (TollStation)TollListBoX.SelectedItem;
            myPassings.Add(new Passing(selectedCar, selectedToll));
            PassingListBoX.ItemsSource = null;
            PassingListBoX.ItemsSource = myPassings;
        }
    }
}
