﻿using System;

public class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Antall ansatte før vi starter: {0}", Ansatt.Count);

        Ansatt a1 = new Ansatt("Eli", "Elvestuen");
        Ansatt a2 = new Ansatt("Bjarne", "Bryn");

        //a2.FirstName = "Bjarte";  // Skal være ugyldig kode.

        Console.WriteLine("\nVi har nå ansatt disse folkene:");
        Console.WriteLine("Ansatt 1: {0}\nAnsatt 2: {1}\n", a1, a2);
        Console.WriteLine("Antall ansatte nå: {0}", Ansatt.Count);

        Console.WriteLine("\nTrykk en tast ...");
        Console.ReadKey();
    }
}
