﻿public class Ansatt
{
    public Ansatt(string firstName, string lastName)
    {
        this.FirstName = firstName;
        this.LastName = lastName;
        Count++;
    }

    public static int Count { get; private set; }
    public string FirstName { get; private set; }
    public string LastName { get; private set; }

    public override string ToString()
    {
        return $"{FirstName} {LastName}";
    }
}