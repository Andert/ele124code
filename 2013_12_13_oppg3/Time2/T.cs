﻿using System;

namespace Tid
{
    public class Tid
    {
        //private int time;   // 0 - 23
        //private int minutt; // 0 - 59
        //private int sekund; // 0 - 59
        // ----  LEGG TIL DIN KODE HER.  ----

        private TimeSpan timeOfDay;
        internal static Tid Now()
        {
            return new Tid(DateTime.Now.TimeOfDay);
        }

        public Tid():this(0,0,0){}
        public Tid(int hour) : this(hour, 0, 0) { }
        public Tid(int hour, int minutes):this(hour,minutes,0){}
        public Tid(int hours, int minutes, int seconds)
        {
            timeOfDay = new TimeSpan(hours, minutes, seconds);
            timeOfDay = timeOfDay.Subtract(TimeSpan.FromDays(timeOfDay.Days));
        }

        public Tid(Tid copy)
        {
            this.timeOfDay = copy.timeOfDay;
        }

        public Tid(TimeSpan timeOfDay)
        {
            this.timeOfDay = timeOfDay;
        }

        public override string ToString()
        {
            return $"{timeOfDay.ToString()}";
        }
    }
}
