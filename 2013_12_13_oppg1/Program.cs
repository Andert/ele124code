﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2013_12_13_oppg1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Contestant> contestants = MakeTestContestants();

            List<PointCard> pointCards = GetTestPointsFromJudges(contestants);
            PrintPointCards(pointCards);
        }

        private static List<PointCard> GetTestPointsFromJudges(List<Contestant> contestants)
        {
            List<PointCard> pointCards = new List<PointCard>();
            foreach (var contestant in contestants)
            {
                PointCard pointCard = new PointCard(contestant);
                for (int i = 0; i < pointCard.Judges; i++)
                {
                    Console.WriteLine($"Can judge #{i} give points for contestant:{contestant}?:");
                    double point;
                    while (!double.TryParse(Console.ReadLine(), out point)) { }
                    pointCard.Points.Add(point);
                }
                pointCards.Add(pointCard);
            }
            return pointCards;
        }

        private static void PrintPointCards(List<PointCard> pointCards)
        {
            foreach (var pointCard in pointCards)
            {
                Console.WriteLine($"Calculated sum: {pointCard.GetCalculatedScore()} \t {pointCard}");
            }
        }

        private static List<Contestant> MakeTestContestants()
        {
            return new List<Contestant>
            {
                new Contestant("Anne"),
                new Contestant("Pål"),
                new Contestant("Per")
            };
        }
    }

    class PointCard
    {
        public int Judges { get; private set; }
        public int SkipBest { get; private set; }
        public int SkipWorst { get; private set; }
        public Contestant Contestant { get; private set; }
        public List<double> Points { get; private set; }
        //List<double> points and just sort it when needed.

        public PointCard(Contestant contestant)
        {
            this.Contestant = contestant;
            Points = new List<double>(); 
            this.Judges = 4;
            this.SkipBest = 1;
            this.SkipWorst = 1;
        }

        public override string ToString()
        {
            string pointsString = "";
            foreach (var point in Points)
            {
                pointsString += point.ToString() + ",";
            }
            pointsString.TrimEnd(',');
            return $"Contestant: {Contestant}, Points in decreasing order: {pointsString}";
        }

        public double GetCalculatedScore()
        {
            Points.Sort(new DescendingComparer());
            int numberOfPointsToTake = Judges - SkipBest - SkipWorst;
            double average = Points.Skip(SkipBest).Take(numberOfPointsToTake).Sum() / numberOfPointsToTake;
            return average;
        }
    }

    internal class DescendingComparer : IComparer<double>
    {
        public int Compare(double x, double y)
        {
            return (int)(y-x); 
        }
    }

    class Contestant
    {
        public string Name { get; private set; }

        public Contestant(string name)
        {
            this.Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
