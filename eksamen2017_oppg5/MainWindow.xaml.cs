﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eksamen2017_oppg5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<double> numberCollection;
        public MainWindow()
        {
            InitializeComponent();
            numberCollection = new ObservableCollection<double>();
            listBox1.ItemsSource = numberCollection;
        }

        private void addNumberBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                numberCollection.Add(Double.Parse(numberBox.Text));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Not a number", "Error", MessageBoxButton.OK);
            }
        }

        private void sumBtn_Click(object sender, RoutedEventArgs e)
        {
            sumtxtbx.Text = numberCollection.Sum().ToString();
        }

        private void maxBtn_Click(object sender, RoutedEventArgs e)
        {
            maxTxtBx.Text = numberCollection.Max().ToString();
        }

        private void sortBtn_Click(object sender, RoutedEventArgs e)
        {
            List<double> sortingList = new List<double>(numberCollection);
            sortingList.Sort();

            numberCollection = new ObservableCollection<double>(sortingList);
            listBox1.ItemsSource = numberCollection;
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                numberCollection.RemoveAt(listBox1.SelectedIndex);
            }
            catch(ArgumentOutOfRangeException oOREx)
            {
                MessageBox.Show("Select a number first", "Error", MessageBoxButton.OK);
            }
        }

        private void numberBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                addNumberBtn_Click(sender, e);
                numberBox.Text = "";
            }
        }
    }
}
