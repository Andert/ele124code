﻿using System;

namespace ClassLibrary56
{
    public class CarWAutoPass:Car
    {
        private int passID;
        public CarWAutoPass(string regNumber, int passID):base(regNumber)
        {
            this.passID = passID;
        }

        public override string ToString()
        {
            return base.ToString() + $" passID:{passID}";
        }
    }

    public class Car
    {
        private string regNumber;

        public Car(string regNumber)
        {
            this.regNumber = regNumber;
        }

        public override string ToString()
        {
            return $"Car#:{regNumber}";
        }
    }
}
