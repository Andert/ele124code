﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary56
{
    public class TollStation
    {
        private int tollID;
        private string location;

        public TollStation(int tollID, string location)
        {
            this.tollID = tollID;
            this.location = location;
        }
        public override string ToString()
        {
            return $"{tollID} - {location}";
        }
    }
}
