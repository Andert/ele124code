﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary56
{
    public class PassingWAutoPass : Passing //Could delete this class and only use Passing, and the type of car would only matter.
    {
        public PassingWAutoPass(CarWAutoPass car, TollStation station):base((Car)car,station){}
    }

    public class PassingWOAutoPass : Passing
    {
        string photo;
        public PassingWOAutoPass(Car car, TollStation station, string photo) : base((Car)car, station)
        {
            this.photo = photo;
        }
        public override string ToString()
        {
            return base.ToString()+ $" No Auto Pass, photo:{this.photo}";
        }
    }

    public class Passing
    {
        Car car;
        DateTime time;
        public Passing(Car car, TollStation station)
        {
            this.Station = station;
            this.car = car;
            time = DateTime.Now;
        }

        public TollStation Station { get; }

        public override string ToString()
        {
            return $"{time}- car:{car}"; 
        }
    }
}
