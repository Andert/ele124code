﻿using System;
using System.Collections.Generic;

namespace Oppg4_2016106
{
    public class Ordre
    {
        static private int sId = 0;
        public int Id { get; private set; }
        public DateTime DateTime { get; private set; }
        public double Price { get {
                double p = 0;
                foreach ( OrdreLinje ol in OrdreLinjer)
                {
                    p += ol.Price;
                }
                return p;
                    } }
        public Ordre()
        {
            OrdreLinjer = new List<OrdreLinje>();
            this.Id = sId++;
            this.DateTime = DateTime.Now;
        }

        public List<OrdreLinje> OrdreLinjer { get; private set; }
       // public DateTime DateTime { get => dateTime; }

        public override string ToString()
        {
            string returnString = $"Date: {DateTime.ToString()}, OrdreId:{this.Id} : ";
            foreach (OrdreLinje ordreL in OrdreLinjer)
            {
                returnString += ordreL.ToString()+", ";
            }
            return returnString;
        }
    }
}