﻿namespace Oppg4_2016106
{
    public class OrdreLinje
    {
        public int Antall { get; private set; }
        public Produkt Produkt { get; private set; }
        public double Price { get
            {
                return this.Antall * this.Produkt.Price; 
            }
        }

        public OrdreLinje(int antall, Produkt p1)
        {
            this.Antall = antall;
            this.Produkt = p1;
        }
        

        public override string ToString()
        {
            return $"Antall: {Antall}, Produkt:{Produkt}";
        }
    }
}