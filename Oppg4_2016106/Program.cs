﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oppg4_2016106
{
    class Program
    {
        static void Main(string[] args)
        {
            Ordre myOrdre = new Ordre();
            Produkt pB = new Produkt("banan", 6.50);
            int antall = 5;
            myOrdre.OrdreLinjer.Add(new OrdreLinje(antall, pB));
            Produkt produktEple = new Produkt("Eple", 2.70);
            myOrdre.OrdreLinjer.Add(new OrdreLinje(antall, produktEple));

            produktEple.Price = 4.50;
            Console.WriteLine(myOrdre.ToString());
        }
    }
}
