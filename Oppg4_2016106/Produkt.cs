﻿namespace Oppg4_2016106
{
    public class Produkt
    {
        public string Name { get; private set; }
        public double Price { get; set; }

        public Produkt(string name, double price)
        {
            this.Name = name;
            this.Price = price;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}