﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleSandbox
{
    class Test
    {
        public static void Main(string[] args)
        {
            int[] numbers = { 420, 42, 423 };
            Array.Sort(numbers, CompareInt);
            foreach (var item in numbers)
            {
                Console.Write(item);
            }
        }

        public static int CompareInt(int i1, int i2)
        {
            int log1 = (int)Math.Log10(i1);
            int log2 = (int)Math.Log10(i2);
            int sum1 = i1 * ((int)Math.Pow(10, log2+1)) + i2;
            int sum2 = i1 + i2 * ((int)Math.Pow(10, log1+1));
            return sum2 - sum1;
        }
    }
}
